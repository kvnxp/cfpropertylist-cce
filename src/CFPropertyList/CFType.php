<?php
/**
 * LICENSE
 *
 * This file is part of CFPropertyList.
 *
 * Copyright (c) 2018 Teclib'
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 * ------------------------------------------------------------------------------
 * @author    Rodney Rehm <rodney.rehm@medialize.de>
 * @author    Christian Kruse <cjk@wwwtech.de>
 * @copyright Copyright © 2018 Teclib
 * @package   CFPropertyList
 * @license   MIT
 * @link      https://github.com/TECLIB/CFPropertyList/
 * @link      http://developer.apple.com/documentation/Darwin/Reference/ManPages/man5/plist.5.html Property Lists
 * ------------------------------------------------------------------------------
 */

namespace CFPropertyList;

use \DOMDocument;

/**
 * Base-Class of all CFTypes used by CFPropertyList.
 *
 * @example example-create-01.php Using the CFPropertyList API
 * @example example-create-02.php Using CFPropertyList::guess()
 * @example example-create-03.php Using CFPropertyList::guess() with {@link CFDate} and {@link CFData}
 */
abstract class CFType {
  /**
   * CFType nodes
   * @var mixed
   */
    protected $value = null;

    /**
     * CFType as string
     * @var string
     */
    protected $type_string = '';

  /**
   * Create new CFType
   *
   * @param mixed $value Value of CFType
   */
    public function __construct($value = null) {
        $this->setValue($value);
        $this->setType();
    }

  /************************************************************************************************
   *    M A G I C   P R O P E R T I E S
   ************************************************************************************************/

  /**
   * Get the CFType's value
   *
   * @return mixed CFType's value
   */
    public function getValue() {
        return $this->value;
    }

  /**
   * Set the CFType's value
   *
   * @param mixed
   *
   * @return void
   */
    public function setValue($value) {
        $this->value = $value;
    }

    /**
     * Get the type of value as string
     *
     * @return string
     */
    public function getType() {
        return $this->type_string;
    }

  /************************************************************************************************
   *    S E R I A L I Z I N G
   ************************************************************************************************/

  /**
   * Get XML-Node.
   *
   * @param DOMDocument $doc DOMDocument to create DOMNode in
   * @param string $nodeName Name of element to create
   *
   * @return \DOMElement Node created based on CType
   *
   * @uses $value as nodeValue
   */
    public function toXML(DOMDocument $doc, $nodeName = "") {
        $node = $doc->createElement($nodeName);
        $text = $doc->createTextNode($this->value);

        $node->appendChild($text);

        return $node;
    }

  /**
   * Get CFType's value.
   *
   * @return CFType
   */
    public function toArray() {
        return $this;
    }

    /**
     * Get the type of the value as string
     */
    protected function setType() {
        switch (true) {
            case $this instanceof CFBoolean:
                $this->type_string = 'bool';
                break;
            case $this instanceof CFString:
                $this->type_string = 'string';
                break;
            case $this instanceof CFData:
                $this->type_string = 'data';
                break;
            case $this instanceof CFNumber:
                $this->type_string = is_float($this->getValue()) ? 'real':'integer';
                break;
            case $this instanceof CFDate:
                $this->type_string = 'date';
                break;
            case $this instanceof CFDictionary:
                $this->type_string = 'dictionary';
                break;
            case $this instanceof CFArray:
                $this->type_string = 'array';
                break;
            case $this instanceof CFUid:
                $this->type_string = 'uid';
                break;
            default:
                $this->type_string = 'unknown';
                break;
        }
    }
}
